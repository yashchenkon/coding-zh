CREATE TABLE counter (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    village_name VARCHAR (255) NOT NULL UNIQUE
);

CREATE TABLE counter_increase_history (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    counter_id BIGINT NOT NULL,
    amount DECIMAL NOT NULL,
    created_at TIMESTAMP NOT NULL,
    CONSTRAINT fk_counter_increase_history_counter_id FOREIGN KEY (counter_id) REFERENCES counter (id)
);

CREATE INDEX idx_counter_increase_history_counter_id_created_at ON counter_increase_history (counter_id, created_at);