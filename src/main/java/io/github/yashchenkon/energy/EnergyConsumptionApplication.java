package io.github.yashchenkon.energy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;

@SpringBootApplication
@EnableJdbcRepositories(basePackages = "io.github.yashchenkon.energy.domain.repository")
public class EnergyConsumptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnergyConsumptionApplication.class, args);
	}

}
