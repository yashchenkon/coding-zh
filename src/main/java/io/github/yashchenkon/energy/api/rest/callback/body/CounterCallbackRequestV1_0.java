package io.github.yashchenkon.energy.api.rest.callback.body;

import com.fasterxml.jackson.annotation.JsonCreator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CounterCallbackRequestV1_0 {
    @NotNull
    private final Long counterId;

    @NotNull
    @Min(value = 0)
    private final Double amount;

    @JsonCreator
    public CounterCallbackRequestV1_0(Long counterId, Double amount) {
        this.counterId = counterId;
        this.amount = amount;
    }

    public Long getCounterId() {
        return counterId;
    }

    public Double getAmount() {
        return amount;
    }
}
