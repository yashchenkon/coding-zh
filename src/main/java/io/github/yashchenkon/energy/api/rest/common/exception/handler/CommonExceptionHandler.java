package io.github.yashchenkon.energy.api.rest.common.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.relational.core.conversion.DbActionExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import io.github.yashchenkon.energy.api.rest.common.body.ErrorResponse;
import io.github.yashchenkon.energy.domain.exception.CounterNotFoundException;

@RestControllerAdvice
public class CommonExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonExceptionHandler.class);

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CounterNotFoundException.class)
    public ErrorResponse handleCounterNotFoundException(CounterNotFoundException ex) {
        LOGGER.error(ex.getMessage(), ex);

        return new ErrorResponse("Counter not found");
    }

    @ExceptionHandler(DbActionExecutionException.class)
    public ResponseEntity<ErrorResponse> handleDbActionExecutionException(DbActionExecutionException ex) {
        LOGGER.error(ex.getMessage(), ex);

        if (ex.getCause().getMessage().contains("FK_COUNTER_INCREASE_HISTORY_COUNTER_ID")) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorResponse("Counter not found"));
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(handleException(ex));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        LOGGER.error(ex.getMessage(), ex);

        FieldError fieldError = ex.getBindingResult().getFieldError();
        return new ErrorResponse("Field '" + fieldError + "' " + fieldError.getDefaultMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleException(Exception ex) {
        LOGGER.error(ex.getMessage(), ex);

        return new ErrorResponse("General error");
    }
}
