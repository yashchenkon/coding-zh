package io.github.yashchenkon.energy.api.rest.callback.adapter;

import org.springframework.stereotype.Component;

import io.github.yashchenkon.energy.api.rest.callback.body.CounterCallbackRequestV1_0;
import io.github.yashchenkon.energy.domain.service.history.CounterIncreaseHistoryService;
import io.github.yashchenkon.energy.domain.service.history.dto.IncreaseCounterDto;

@Component
public class CounterCallBackRestAdapterV1 {

    private final CounterIncreaseHistoryService counterIncreaseHistoryService;

    public CounterCallBackRestAdapterV1(CounterIncreaseHistoryService counterIncreaseHistoryService) {
        this.counterIncreaseHistoryService = counterIncreaseHistoryService;
    }

    public void callback(CounterCallbackRequestV1_0 request) {
        counterIncreaseHistoryService.increase(new IncreaseCounterDto(request.getCounterId(), request.getAmount()));
    }
}
