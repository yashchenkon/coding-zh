package io.github.yashchenkon.energy.api.rest.common.body;

import com.fasterxml.jackson.annotation.JsonCreator;

public class EntityCreatedResponseV1_0 {
    private final Long id;

    @JsonCreator
    public EntityCreatedResponseV1_0(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
