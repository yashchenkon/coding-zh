package io.github.yashchenkon.energy.api.rest.report.body;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.List;

public class ConsumptionReportResponseBodyV1_0 {
    private final List<ConsumptionReportItemResponseBodyV1_0> villages;

    @JsonCreator
    public ConsumptionReportResponseBodyV1_0(List<ConsumptionReportItemResponseBodyV1_0> villages) {
        this.villages = villages;
    }

    public List<ConsumptionReportItemResponseBodyV1_0> getVillages() {
        return villages;
    }
}
