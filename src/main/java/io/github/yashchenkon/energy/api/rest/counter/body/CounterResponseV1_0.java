package io.github.yashchenkon.energy.api.rest.counter.body;

public class CounterResponseV1_0 {
    private final Long id;
    private final String villageName;

    public CounterResponseV1_0(Long id, String villageName) {
        this.id = id;
        this.villageName = villageName;
    }

    public Long getId() {
        return id;
    }

    public String getVillageName() {
        return villageName;
    }
}
