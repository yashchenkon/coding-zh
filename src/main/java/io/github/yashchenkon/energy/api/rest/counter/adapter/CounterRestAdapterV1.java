package io.github.yashchenkon.energy.api.rest.counter.adapter;

import org.springframework.stereotype.Component;

import io.github.yashchenkon.energy.api.rest.common.body.EntityCreatedResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CounterResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CreateCounterRequestV1_0;
import io.github.yashchenkon.energy.domain.model.counter.Counter;
import io.github.yashchenkon.energy.domain.service.counter.CounterService;
import io.github.yashchenkon.energy.domain.service.counter.dto.CreateCounterDto;

@Component
public class CounterRestAdapterV1 {

    private final CounterService counterService;

    public CounterRestAdapterV1(CounterService counterService) {
        this.counterService = counterService;
    }

    public EntityCreatedResponseV1_0 create(CreateCounterRequestV1_0 request) {
        Long id = counterService.create(new CreateCounterDto(request.getVillageName()));

        return new EntityCreatedResponseV1_0(id);
    }

    public CounterResponseV1_0 getById(Long id) {
        Counter counter = counterService.getById(id);

        return new CounterResponseV1_0(
            counter.getId(), counter.getVillageName()
        );
    }
}
