package io.github.yashchenkon.energy.api.rest.counter;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.yashchenkon.energy.api.rest.common.body.EntityCreatedResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.adapter.CounterRestAdapterV1;
import io.github.yashchenkon.energy.api.rest.counter.body.CounterResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CreateCounterRequestV1_0;

@RestController
@RequestMapping("/counter")
public class CounterRestControllerV1 {

    private final CounterRestAdapterV1 adapter;

    public CounterRestControllerV1(CounterRestAdapterV1 adapter) {
        this.adapter = adapter;
    }

    @PostMapping
    public EntityCreatedResponseV1_0 create(@RequestBody @Validated CreateCounterRequestV1_0 request) {
        return adapter.create(request);
    }

    @GetMapping
    public CounterResponseV1_0 getById(@RequestParam Long id) {
        return adapter.getById(id);
    }

}
