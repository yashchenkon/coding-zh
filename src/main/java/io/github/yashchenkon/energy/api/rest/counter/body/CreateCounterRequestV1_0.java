package io.github.yashchenkon.energy.api.rest.counter.body;

import com.fasterxml.jackson.annotation.JsonCreator;

import javax.validation.constraints.NotBlank;

public class CreateCounterRequestV1_0 {
    @NotBlank
    private final String villageName;

    @JsonCreator
    public CreateCounterRequestV1_0(String villageName) {
        this.villageName = villageName;
    }

    public String getVillageName() {
        return villageName;
    }
}
