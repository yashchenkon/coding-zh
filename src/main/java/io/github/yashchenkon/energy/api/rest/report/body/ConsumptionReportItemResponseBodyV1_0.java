package io.github.yashchenkon.energy.api.rest.report.body;

import com.fasterxml.jackson.annotation.JsonCreator;

public class ConsumptionReportItemResponseBodyV1_0 {
    private final String villageName;
    private final Double consumption;

    @JsonCreator
    public ConsumptionReportItemResponseBodyV1_0(String villageName, Double consumption) {
        this.villageName = villageName;
        this.consumption = consumption;
    }

    public String getVillageName() {
        return villageName;
    }

    public Double getConsumption() {
        return consumption;
    }
}
