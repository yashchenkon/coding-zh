package io.github.yashchenkon.energy.api.rest.callback;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.yashchenkon.energy.api.rest.callback.adapter.CounterCallBackRestAdapterV1;
import io.github.yashchenkon.energy.api.rest.callback.body.CounterCallbackRequestV1_0;

@RestController
@RequestMapping("/counter_callback")
public class CounterCallBackRestControllerV1 {

    private final CounterCallBackRestAdapterV1 adapter;

    public CounterCallBackRestControllerV1(CounterCallBackRestAdapterV1 adapter) {
        this.adapter = adapter;
    }

    @PostMapping
    public void callback(@RequestBody @Validated CounterCallbackRequestV1_0 request) {
        adapter.callback(request);
    }
}
