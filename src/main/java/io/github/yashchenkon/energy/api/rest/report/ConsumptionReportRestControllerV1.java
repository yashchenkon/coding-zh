package io.github.yashchenkon.energy.api.rest.report;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.yashchenkon.energy.api.rest.report.adapter.ConsumptionReportRestAdapterV1;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportResponseBodyV1_0;

import java.time.Duration;

@RestController
@RequestMapping("/consumption_report")
public class ConsumptionReportRestControllerV1 {

    private final ConsumptionReportRestAdapterV1 adapter;

    public ConsumptionReportRestControllerV1(ConsumptionReportRestAdapterV1 adapter) {
        this.adapter = adapter;
    }

    @GetMapping
    public ConsumptionReportResponseBodyV1_0 reportFor(@RequestParam String duration) {
        return adapter.consumptionReportFor(
            Duration.parse("PT" + duration.toUpperCase())
        );
    }
}
