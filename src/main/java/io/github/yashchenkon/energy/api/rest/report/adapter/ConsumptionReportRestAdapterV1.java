package io.github.yashchenkon.energy.api.rest.report.adapter;

import org.springframework.stereotype.Component;

import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportItemResponseBodyV1_0;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportResponseBodyV1_0;
import io.github.yashchenkon.energy.domain.model.report.ConsumptionReport;
import io.github.yashchenkon.energy.domain.service.report.ConsumptionReportService;

import java.time.Duration;
import java.util.stream.Collectors;

@Component
public class ConsumptionReportRestAdapterV1 {

    private final ConsumptionReportService consumptionReportService;

    public ConsumptionReportRestAdapterV1(ConsumptionReportService consumptionReportService) {
        this.consumptionReportService = consumptionReportService;
    }

    public ConsumptionReportResponseBodyV1_0 consumptionReportFor(Duration duration) {
        ConsumptionReport consumptionReport = consumptionReportService.reportForLast(duration);

        return new ConsumptionReportResponseBodyV1_0(
            consumptionReport.getItems()
                .stream()
                .map(item -> new ConsumptionReportItemResponseBodyV1_0(item.getVillageName(), item.getConsumption()))
                .collect(Collectors.toList())
        );
    }
}
