package io.github.yashchenkon.energy.domain.model.history;

import org.springframework.data.annotation.Id;

import java.time.Instant;
import java.util.StringJoiner;

public class CounterIncreaseHistory {
    @Id
    private Long id;
    private final Long counterId;
    private final Double amount;
    private final Instant createdAt;

    public CounterIncreaseHistory(Long id, Long counterId, Double amount, Instant createdAt) {
        this.id = id;
        this.counterId = counterId;
        this.amount = amount;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public Long getCounterId() {
        return counterId;
    }

    public Double getAmount() {
        return amount;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CounterIncreaseHistory.class.getSimpleName() + "[", "]")
            .add("id=" + id)
            .add("counterId=" + counterId)
            .add("amount=" + amount)
            .toString();
    }
}
