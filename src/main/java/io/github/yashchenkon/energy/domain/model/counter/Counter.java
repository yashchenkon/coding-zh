package io.github.yashchenkon.energy.domain.model.counter;

import org.springframework.data.annotation.Id;

import java.util.StringJoiner;

public class Counter {
    @Id
    private Long id;
    private final String villageName;

    public Counter(Long id, String villageName) {
        this.id = id;
        this.villageName = villageName;
    }

    public Long getId() {
        return id;
    }

    public String getVillageName() {
        return villageName;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Counter.class.getSimpleName() + "[", "]")
            .add("id=" + id)
            .add("villageName='" + villageName + "'")
            .toString();
    }
}
