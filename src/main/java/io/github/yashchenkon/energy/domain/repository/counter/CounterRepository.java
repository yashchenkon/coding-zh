package io.github.yashchenkon.energy.domain.repository.counter;

import org.springframework.data.repository.CrudRepository;

import io.github.yashchenkon.energy.domain.model.counter.Counter;

public interface CounterRepository extends CrudRepository<Counter, Long> {
}
