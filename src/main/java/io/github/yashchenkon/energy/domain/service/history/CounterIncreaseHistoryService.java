package io.github.yashchenkon.energy.domain.service.history;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.yashchenkon.energy.domain.model.history.CounterIncreaseHistory;
import io.github.yashchenkon.energy.domain.repository.history.CounterIncreaseHistoryRepository;
import io.github.yashchenkon.energy.domain.service.history.dto.IncreaseCounterDto;

import java.time.Instant;

@Service
public class CounterIncreaseHistoryService {

    private final CounterIncreaseHistoryRepository counterIncreaseHistoryRepository;

    public CounterIncreaseHistoryService(CounterIncreaseHistoryRepository counterIncreaseHistoryRepository) {
        this.counterIncreaseHistoryRepository = counterIncreaseHistoryRepository;
    }

    @Transactional
    public Long increase(IncreaseCounterDto increaseCounter) {
        CounterIncreaseHistory event = counterIncreaseHistoryRepository.save(
            new CounterIncreaseHistory(null, increaseCounter.getCounterId(), increaseCounter.getAmount(), Instant.now()));

        return event.getId();
    }
}
