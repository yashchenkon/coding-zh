package io.github.yashchenkon.energy.domain.service.counter;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.yashchenkon.energy.domain.exception.CounterNotFoundException;
import io.github.yashchenkon.energy.domain.model.counter.Counter;
import io.github.yashchenkon.energy.domain.repository.counter.CounterRepository;
import io.github.yashchenkon.energy.domain.service.counter.dto.CreateCounterDto;

@Service
public class CounterService {

    private final CounterRepository counterRepository;

    public CounterService(CounterRepository counterRepository) {
        this.counterRepository = counterRepository;
    }

    @Transactional
    public Long create(CreateCounterDto createCounter) {
        Counter result = counterRepository.save(new Counter(null, createCounter.getVillageName()));
        return result.getId();
    }

    @Transactional(readOnly = true)
    public Counter getById(Long id) {
        return counterRepository.findById(id)
            .orElseThrow(CounterNotFoundException::new);
    }
}
