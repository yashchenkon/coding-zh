package io.github.yashchenkon.energy.domain.service.report;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.yashchenkon.energy.domain.model.report.ConsumptionReport;
import io.github.yashchenkon.energy.domain.repository.history.CounterIncreaseHistoryRepository;

import java.time.Duration;
import java.time.Instant;

@Service
public class ConsumptionReportService {

    private final CounterIncreaseHistoryRepository counterIncreaseHistoryRepository;

    public ConsumptionReportService(CounterIncreaseHistoryRepository counterIncreaseHistoryRepository) {
        this.counterIncreaseHistoryRepository = counterIncreaseHistoryRepository;
    }

    @Transactional(readOnly = true)
    public ConsumptionReport reportForLast(Duration duration) {
        Instant startDate = Instant.now().minus(duration);

        return new ConsumptionReport(
            counterIncreaseHistoryRepository.reportFrom(startDate)
        );
    }
}
