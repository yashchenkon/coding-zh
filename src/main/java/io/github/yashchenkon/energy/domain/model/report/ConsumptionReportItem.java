package io.github.yashchenkon.energy.domain.model.report;

import java.util.StringJoiner;

public class ConsumptionReportItem {
    private final String villageName;
    private final Double consumption;

    public ConsumptionReportItem(String villageName, Double consumption) {
        this.villageName = villageName;
        this.consumption = consumption;
    }

    public String getVillageName() {
        return villageName;
    }

    public Double getConsumption() {
        return consumption;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ConsumptionReportItem.class.getSimpleName() + "[", "]")
            .add("villageName='" + villageName + "'")
            .add("amount=" + consumption)
            .toString();
    }
}
