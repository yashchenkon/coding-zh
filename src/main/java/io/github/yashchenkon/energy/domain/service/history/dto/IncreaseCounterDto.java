package io.github.yashchenkon.energy.domain.service.history.dto;

public class IncreaseCounterDto {
    private final Long counterId;
    private final Double amount;

    public IncreaseCounterDto(Long counterId, Double amount) {
        this.counterId = counterId;
        this.amount = amount;
    }

    public Long getCounterId() {
        return counterId;
    }

    public Double getAmount() {
        return amount;
    }
}
