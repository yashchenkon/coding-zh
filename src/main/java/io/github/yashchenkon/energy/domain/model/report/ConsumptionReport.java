package io.github.yashchenkon.energy.domain.model.report;

import java.util.List;

public class ConsumptionReport {
    private final List<ConsumptionReportItem> items;

    public ConsumptionReport(List<ConsumptionReportItem> items) {
        this.items = items;
    }

    public List<ConsumptionReportItem> getItems() {
        return items;
    }
}
