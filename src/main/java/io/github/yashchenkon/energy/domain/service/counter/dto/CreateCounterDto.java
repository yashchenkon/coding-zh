package io.github.yashchenkon.energy.domain.service.counter.dto;

public class CreateCounterDto {
    private final String villageName;

    public CreateCounterDto(String villageName) {
        this.villageName = villageName;
    }

    public String getVillageName() {
        return villageName;
    }
}
