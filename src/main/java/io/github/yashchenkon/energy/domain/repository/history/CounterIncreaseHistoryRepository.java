package io.github.yashchenkon.energy.domain.repository.history;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;

import io.github.yashchenkon.energy.domain.model.history.CounterIncreaseHistory;
import io.github.yashchenkon.energy.domain.model.report.ConsumptionReportItem;

import java.time.Instant;
import java.util.List;

public interface CounterIncreaseHistoryRepository extends CrudRepository<CounterIncreaseHistory, Long> {

    @Query("select c.village_name as village_name, coalesce(sum(cih.amount), 0) as consumption from counter c left join counter_increase_history cih on c.id = cih.counter_id where cih.created_at >= :startFrom group by c.village_name")
    List<ConsumptionReportItem> reportFrom(Instant startFrom);
}
