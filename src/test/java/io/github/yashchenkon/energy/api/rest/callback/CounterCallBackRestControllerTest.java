package io.github.yashchenkon.energy.api.rest.callback;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import io.github.yashchenkon.energy.api.rest.AbstractRestControllerTest;
import io.github.yashchenkon.energy.api.rest.callback.body.CounterCallbackRequestV1_0;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportItemResponseBodyV1_0;

import java.util.Objects;
import java.util.UUID;

public class CounterCallBackRestControllerTest extends AbstractRestControllerTest {

    @Test
    public void shouldPostCallBack() throws Exception {
        String villageName = UUID.randomUUID().toString();
        Long counterId = createCounter(villageName);

        double amount = 100.0;
        postCallBackFor(counterId, amount);

        ConsumptionReportItemResponseBodyV1_0 report = consumptionReport().getVillages().stream()
            .filter(item -> Objects.equals(villageName, item.getVillageName()))
            .findFirst()
            .orElse(null);

        assertThat(report).isNotNull();
        assertThat(report.getConsumption()).isEqualTo(amount);
    }

    @Test
    public void shouldReturnErrorOnPostCallBackIfCounterDoesNotExist() throws Exception {
        double amount = 100.0;
        CounterCallbackRequestV1_0 request = new CounterCallbackRequestV1_0(-10L, amount);

        mockMvc.perform(
            post("/counter_callback")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void shouldReturnErrorOnPostCallBackIfCounterNegative() throws Exception {
        String villageName = UUID.randomUUID().toString();
        Long counterId = createCounter(villageName);

        double amount = -100.0;
        CounterCallbackRequestV1_0 request = new CounterCallbackRequestV1_0(counterId, amount);

        mockMvc.perform(
            post("/counter_callback")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void shouldReturnErrorOnPostCallBackIfCounterIsNull() throws Exception {
        double amount = 100.0;
        CounterCallbackRequestV1_0 request = new CounterCallbackRequestV1_0(null, amount);

        mockMvc.perform(
            post("/counter_callback")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void shouldReturnErrorOnPostCallBackIfAmountIsNull() throws Exception {
        String villageName = UUID.randomUUID().toString();
        Long counterId = createCounter(villageName);

        CounterCallbackRequestV1_0 request = new CounterCallbackRequestV1_0(counterId, null);

        mockMvc.perform(
            post("/counter_callback")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }
}
