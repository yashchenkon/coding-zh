package io.github.yashchenkon.energy.api.rest.report;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.data.util.Pair;

import io.github.yashchenkon.energy.api.rest.AbstractRestControllerTest;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportItemResponseBodyV1_0;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ConsumptionReportRestControllerTest extends AbstractRestControllerTest {

    @Test
    public void shouldReturnConsumptionReport() throws Exception {
        List<Pair<Long, String>> counters = IntStream.range(0, 10)
            .mapToObj($ -> {
                try {
                    String villageName = UUID.randomUUID().toString();
                    return Pair.of(createCounter(villageName), villageName);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            })
            .collect(Collectors.toList());

        for (int i = 0; i < counters.size(); i++) {
            postCallBackFor(counters.get(i).getFirst(), i * 100.0);
            postCallBackFor(counters.get(i).getFirst(), i * 1000.0);
        }

        List<ConsumptionReportItemResponseBodyV1_0> consumptionReport = consumptionReport().getVillages();
        for (int i = 0; i < counters.size(); i++) {
            int finalI = i;
            ConsumptionReportItemResponseBodyV1_0 item = consumptionReport.stream()
                .filter(it -> Objects.equals(it.getVillageName(), counters.get(finalI).getSecond()))
                .findFirst()
                .orElse(null);

            assertThat(item).isNotNull();
            assertThat(item.getConsumption()).isEqualTo(i * 100.0 + i * 1000.0);
        }
    }
}
