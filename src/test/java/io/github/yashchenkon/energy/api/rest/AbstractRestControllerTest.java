package io.github.yashchenkon.energy.api.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import io.github.yashchenkon.energy.EnergyConsumptionApplication;
import io.github.yashchenkon.energy.api.rest.callback.body.CounterCallbackRequestV1_0;
import io.github.yashchenkon.energy.api.rest.common.body.EntityCreatedResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CounterResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CreateCounterRequestV1_0;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportResponseBodyV1_0;

@AutoConfigureMockMvc
@SpringBootTest(classes = EnergyConsumptionApplication.class)
public class AbstractRestControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    protected Long createCounter(String villageName) throws Exception {
        CreateCounterRequestV1_0 request = new CreateCounterRequestV1_0(villageName);

        String response = mockMvc.perform(
            post("/counter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andReturn()
            .getResponse()
            .getContentAsString();

        return objectMapper.readValue(response, EntityCreatedResponseV1_0.class).getId();
    }

    protected CounterResponseV1_0 getCounterById(Long id) throws Exception {
        String response = mockMvc.perform(
            get("/counter?id={id}", id)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        return objectMapper.readValue(response, CounterResponseV1_0.class);
    }

    protected ConsumptionReportResponseBodyV1_0 consumptionReport() throws Exception {
        String response = mockMvc.perform(
            get("/consumption_report?duration=24h")
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        return objectMapper.readValue(response, ConsumptionReportResponseBodyV1_0.class);
    }

    protected void postCallBackFor(Long counterId, Double amount) throws Exception {
        CounterCallbackRequestV1_0 request = new CounterCallbackRequestV1_0(counterId, amount);

        mockMvc.perform(
            post("/counter_callback")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isOk());
    }
}
