package io.github.yashchenkon.energy.api.rest.counter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import io.github.yashchenkon.energy.api.rest.AbstractRestControllerTest;
import io.github.yashchenkon.energy.api.rest.counter.body.CounterResponseV1_0;
import io.github.yashchenkon.energy.api.rest.counter.body.CreateCounterRequestV1_0;
import io.github.yashchenkon.energy.api.rest.report.body.ConsumptionReportResponseBodyV1_0;

import java.util.UUID;

public class CounterRestControllerTest extends AbstractRestControllerTest {

    @Test
    public void shouldCreateCounter() throws Exception {
        String villageName = UUID.randomUUID().toString();

        Long counterId = createCounter(villageName);

        CounterResponseV1_0 counter = getCounterById(counterId);
        assertThat(counter.getVillageName()).isEqualTo(villageName);

        ConsumptionReportResponseBodyV1_0 consumptionReport = consumptionReport();
        boolean presentInReport = consumptionReport.getVillages().stream()
            .anyMatch(item -> item.getVillageName().equals(villageName));
        assertThat(presentInReport).isFalse();
    }

    @Test
    public void shouldReturnErrorOnCreateWithBlankVillage() throws Exception {
        CreateCounterRequestV1_0 request = new CreateCounterRequestV1_0(null);

        mockMvc.perform(
            post("/counter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    objectMapper.writeValueAsBytes(request)
                )
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    public void shouldReturnCounterById() throws Exception {
        String villageName = UUID.randomUUID().toString();
        Long counterId = createCounter(villageName);

        mockMvc.perform(
            get("/counter?id={id}", counterId)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(Matchers.equalTo(counterId.intValue())))
            .andExpect(jsonPath("$.village_name").value(Matchers.equalTo(villageName)));
    }

    @Test
    public void shouldReturnErrorOnGetByIdNotExistent() throws Exception {
        mockMvc.perform(
            get("/counter?id={id}", -10)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.message").isNotEmpty());
    }
}
