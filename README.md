### Execute

To run build, execute:
```
./gradlew build
```

To run service, execute:
```
./gradlew bootRun
```
### Task

You have to design and build the system that allows to receive and collect data about energy consumption from different villages. As a result, your system should, on demand, give out the consumption report per village for the last 24h. As a result of your work, we expect the end-to-end design of the system (a model, system architecture, technology, and frameworks choice, testing strategy, etc.). We would also like to see your code for the whole system or reasonable part of it. Since our main programming language is Java, it would be nice if you implement this solution using it. 
However, if you have a project (with a task of similar complexity), then you can share it with us as well, instead of coding this task. In this case please create a statement/document explaining the problem that is being solved with it, this project should speaks for your code skills :-).

Consider that your system has an API that is called by electricity counters: 

```
POST /counter_callback 
{
    "counter_id": "1",
    "amount": 10000.123
}
```

To get information additional information about the counter you have to call the following external API: 

```
GET /counter?id=1 
{
    "id": "1",
    "village_name": "Villarriba"
}
```

As a result, it's expected that your system will expose the following API: 

```
GET /consumption_report?duration=24h 
{
    "villages": [
        {
            "village_name": "Villarriba",
            "consumption": 12345.123
        },
        {
            "village_name": "Villabajo",
            "consumption": 23456.123
        }
    ]
}   
``` 

### Assumptions

1. Counter represents the total energy consumption PER single village. Since we have a requirement
to implement an API returning report for energy consumption during last 24 hours for ALL counters we have,
I assume the number of counter is not huge (say 10000) so it can be returned in a response for a single API call.

2. Let's assume that each counter is periodically increased by some amount of energy. IOT devices are sending us
the AGGREGATED details of the energy consumption for small period of time (say total consumption per village during last 1 hour
from a single device).  Therefore we have a peak load of 10000 requests per second (when all devices send us new data at once).

3. Such a pick load might be easily handled by relational database. Relational databases also provide us an ability to
set up constraints out of the box (like checking that counter exists by setting up a foreign key, etc). So it prevents
us from doing some extra calls to DB. Of course, some requests will be a bit delayed due to limited amount of connections
to DB, but I think we are good with it as far as we have no constant load to our service.

4. I assume the data should be processed in real time - if electricity counter received 200 OK, the subsequent call for a 
consumption report will include the submitted data immediately.

5. We don't have any API security.

### System increase assumptions

1. If we have lots of counters (say 1 million +), we won't be able to handle such amount of traffic with my solution.
It will needs to be re-worked a little bit in order to handle proper amount of requests.

2. One way that appears in my mind is a job aggregation. We can use some distributed event processing system (like Kafka).
When counter submits data, we put new event into Kafka and it will be processed asynchronously by event consumers. Event
consumers will be aggregating events to process them in batches. Async processing makes our system EVENTUAL and allows us
to handle huge amount of requests without changing internal DB.

3. Another way of how to handle such amount of requests will be to use DB sharding. We might shard our database by village
district, for instance. In this way we split the total amounts of request onto number of shards so we can handle more requests.

4. As number of counters increases, we need to change the reports API as it will be impossible to returns huge amounts of reports
in a single response.

Will be happy to discuss details later.

### Technologies choice

In order to quickly bootstrap the PoC, I will use the following technologies:

- Java 11 (latest LTS release)
- Spring Boot (quick API implementation, DB integration, IoC container)
- H2 in memory DB (storing the data)
- Flyway (applying a schema to DB)
- Gradle (manage dependencies, build everything into single jar)

### Testing strategy

The primary clients of implemented API are electricity counters which will be communicating with the service over the REST.
So I will implement API tests covering our API cases. All tests will be using real instance of the service (with real DB / etc).